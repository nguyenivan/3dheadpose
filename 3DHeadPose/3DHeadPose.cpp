﻿#include "stdafx.h"
#include <Windows.h>
#include <Kinect.h>
#include <opencv2/opencv.hpp>
#include <fstream>

#include "iostream"

#define rowPtr(imagePtr, dataType, lineIndex) \
	(dataType *)(imagePtr->imageData + (lineIndex)* imagePtr->widthStep)


using namespace std;
using namespace cv;

template<class Interface>

inline void SafeRelease(Interface *& pInterfaceToRelease)
{
	if (pInterfaceToRelease != NULL){
		pInterfaceToRelease->Release();
		pInterfaceToRelease = NULL;
	}
}
int GetMinor(long double **src, long double **dest, int row, int col, int order)
{
	// indicate which col and row is being copied to dest
	int colCount = 0, rowCount = 0;

	for (int i = 0; i < order; i++)
	{
		if (i != row)
		{
			colCount = 0;
			for (int j = 0; j < order; j++)
			{
				// when j is not the element
				if (j != col)
				{
					dest[rowCount][colCount] = src[i][j];
					colCount++;
				}
			}
			rowCount++;
		}
	}

	return 1;
}
long double CalcDeterminant(long double **mat, int n)
{
	long double d = 0;
	int c, subi, i, j, subj;
	long double **submat = new long double*[n];
	for (int i = 0; i < n; i++)
		submat[i] = new long double[n];
	if (n == 2)
	{
		return((mat[0][0] * mat[1][1]) - (mat[1][0] * mat[0][1]));
	}
	else
	{
		for (c = 0; c < n; c++)
		{
			subi = 0;
			for (i = 1; i < n; i++)
			{
				subj = 0;
				for (j = 0; j < n; j++)
				{
					if (j == c)
					{
						continue;
					}
					submat[subi][subj] = mat[i][j];
					subj++;
				}
				subi++;
			}
			d = d + (pow(-1, c) * mat[0][c] * CalcDeterminant(submat, n - 1));
		}
	}
	return d;
}



void MatrixInversion(long double **A, int order, long double **Y)
{
	// get the determinant of a
	long double det = 1.0 / CalcDeterminant(A, order);

	// memory allocation
	long double *temp = new long double[(order - 1)*(order - 1)];
	long double **minor = new long double*[order - 1];
	for (int i = 0; i < order - 1; i++)
		minor[i] = temp + (i*(order - 1));

	for (int j = 0; j < order; j++)
	{
		for (int i = 0; i < order; i++)
		{
			// get the co-factor (matrix) of A(j,i)
			GetMinor(A, minor, j, i, order);
			Y[i][j] = det*CalcDeterminant(minor, order - 1);
			if ((i + j) % 2 == 1)
				Y[i][j] = -Y[i][j];
		}
	}

	// release memory
	//delete [] minor[0];
	delete[] temp;
	delete[] minor;
}


void matrixMultiply(long double **arr1, long double **arr2, long double **ans, int N, int L, int M)
{
	for (int R = 0; R < N; R++)
	{
		for (int C = 0; C < M; C++)
		{
			ans[R][C] = 0;
			for (int T = 0; T < L; T++)
				ans[R][C] += (arr1[R][T] * arr2[T][C]);
		}
	}
}
void matrixTranspose(long double **arr1, long double **arr2, int N, int M)
{
	for (int R = 0; R < N; R++)
	for (int C = 0; C < M; C++)
		arr2[R][C] = arr1[C][R];
}

void captureData() {
	cv::setUseOptimized(true);

	// Sensor
	IKinectSensor* pSensor;
	HRESULT hResult = S_OK;
	hResult = GetDefaultKinectSensor(&pSensor);
	hResult = pSensor->Open();

	// Source
	IDepthFrameSource* pDepthSourceSub;
	hResult = pSensor->get_DepthFrameSource(&pDepthSourceSub);

	IDepthFrameReader* pDepthReaderSub;
	hResult = pDepthSourceSub->OpenReader(&pDepthReaderSub);

	IFrameDescription* pDepthDescriptionSub;
	hResult = pDepthSourceSub->get_FrameDescription(&pDepthDescriptionSub);

	int depthWidthSub = 0;
	int depthHeightSub = 0;
	pDepthDescriptionSub->get_Width(&depthWidthSub);
	pDepthDescriptionSub->get_Height(&depthHeightSub);
	unsigned int depthBufferSizeSub = depthWidthSub*depthHeightSub*sizeof(unsigned short);
	cv::Mat depthBufferMatSub(depthHeightSub, depthWidthSub, CV_16UC1);
	//UINT16* depthBuffer = nullptr;
	//depthBuffer = new UINT16[depthHeightSub*depthWidthSub];

	//cv::Mat depthBufferMathSub = new UINT16[depthHeightSub*depthWidthSub];

	cv::Mat depthMatSub(depthHeightSub, depthWidthSub, CV_8UC1);
	unsigned short minDepthSub, maxDepthSub;
	pDepthSourceSub->get_DepthMinReliableDistance(&minDepthSub);
	pDepthSourceSub->get_DepthMaxReliableDistance(&maxDepthSub);
	cv::namedWindow("Background Calibration");
	cv::Mat depthBackground;
	int i = 0;

	while (1)
	{

		IDepthFrame* pDepthSub = nullptr;
		hResult = pDepthReaderSub->AcquireLatestFrame(&pDepthSub);
		if (SUCCEEDED(hResult))
		{

			hResult = pDepthSub->AccessUnderlyingBuffer(&depthBufferSizeSub, reinterpret_cast<UINT16**>(&depthBufferMatSub.data));
			if (SUCCEEDED(hResult))
			{
				depthBufferMatSub.convertTo(depthMatSub, CV_8U, (255.0 / (maxDepthSub)));
			}
		}



		if (!depthBackground.empty()) {
			if (++i % 10 == 0) {

				cv::imwrite("image" + to_string(i / 10) + ".bmp", depthMatSub);
			}
			depthMatSub = depthBackground - depthMatSub;
		}

		SafeRelease(pDepthSub);

		cv::imshow("Background Calibration", depthMatSub);

		if (cv::waitKey(30) == VK_ESCAPE)
		{
			if (depthBackground.empty()){
				depthMatSub.copyTo(depthBackground);
				cv::imwrite("background.bmp", depthBackground);
			}
			else {
				break;
			}
		}
	}


	SafeRelease(pDepthSourceSub);
	SafeRelease(pDepthReaderSub);
	SafeRelease(pDepthDescriptionSub);

	if (pSensor)
		pSensor->Close();

	SafeRelease(pSensor);

	cv::destroyAllWindows();

}

void findTopBottom(Mat image, int &top, int &bottom) {
	Mat proj;
	Mat grayImage;
	cv::cvtColor(image, grayImage, CV_RGB2GRAY);
	cv::reduce(grayImage, proj, 1, CV_REDUCE_SUM, CV_32F);

	/*
	ofstream f;
	f.open("example.txt");
	f << proj;
	f.close();
	*/

	vector<int> series(proj.rows);
	series.assign((float*)proj.datastart, (float*)proj.dataend);

	const int valley_window = 8;

	bool foundTop = false;
	bool foundBottom = false;

	for (int i = 0; i < series.size() - valley_window; i++) {

		if (!foundTop) {
			if (series.at(i) >= valley_window) {
				foundTop = true;
				top = i;
			}
		}

		if (i < valley_window) continue;

		if (!foundBottom) {
			bool found = true;
			for (int j = i - valley_window; j < i + valley_window; j++) {
				if (j < i && series.at(j) <= series.at(i)) {
					found = false;
					break;
				}
				if (j > i && series.at(j) < series.at(i)) {
					found = false;
					break;
				}

			}
			if (found) {
				foundBottom = true;
				bottom = i;
			}
		}
		if (foundTop && foundBottom) break;
	}
	if (!foundBottom) {
		bottom = -1;
	}
	if (!foundTop) {
		top = -1;
	}
}


void findLeftRight(Mat image, int &left, int &right) {
	Mat proj;
	Mat grayImage;
	cv::cvtColor(image, grayImage, CV_RGB2GRAY);
	cv::reduce(grayImage, proj, 0, CV_REDUCE_SUM, CV_32F);

	vector<int> series(proj.cols);
	series.assign((float*)proj.datastart, (float*)proj.dataend);

	const int valley_window = 8;

	bool foundLeft = false;
	bool foundRight = false;

	for (int i = 0; i < series.size(); i++) {
		if (series.at(i) >= valley_window) {
			foundLeft = true;
			left = i;
			break;
		}
	}
	if (!foundLeft) left = -1;

	for (int i = series.size() -1; i > left; i--) {
		if (series.at(i) >= valley_window) {
			foundRight = true;
			right = i;
			break;
		}
	}
	if (!foundLeft) left = -1;
	if (!foundRight) right = -1;
}


void subtractImages(string backGroundImagePath, string currentImagePath) {
	string wName = "Subtraction";
	cv::namedWindow(wName);
	double thresh = 10;
	double maxVal = 255;

	Mat backgroundImage = cv::imread(backGroundImagePath);
	Mat currentImage = cv::imread(currentImagePath);
	Mat segmentedImage = backgroundImage - currentImage;

	//cv::imshow(wName, segmentedImage);
	//while (cv::waitKey() != VK_ESCAPE) {}

	Mat kernel = getStructuringElement(MORPH_RECT, Size(5, 5));
	morphologyEx(segmentedImage, segmentedImage, MORPH_OPEN, kernel);
	threshold(segmentedImage, segmentedImage, thresh, maxVal, CV_THRESH_BINARY);

	cv::imshow(wName, segmentedImage);
	while (cv::waitKey() != VK_ESCAPE) {}

	int top, bottom;
	findTopBottom(segmentedImage / maxVal, top, bottom);
	

	Rect headRect(0, top, segmentedImage.cols, bottom - top);
	
	Mat headImage;
	segmentedImage(headRect).copyTo(headImage);
	int left, right;
	findLeftRight(headImage, left, right);

	cout << "Top head: " << top << endl;
	cout << "First alley: " << bottom << endl;
	cout << "Left head: " << left << endl;
	cout << "Right head: " << right << endl;
	line(segmentedImage, Point(0, top), Point(segmentedImage.cols, top), Scalar(255, 0, 0));
	line(segmentedImage, Point(0, bottom), Point(segmentedImage.cols, bottom), Scalar(255, 0, 0));
	line(segmentedImage, Point(left, 0), Point(left, segmentedImage.rows), Scalar(255, 0, 0));
	line(segmentedImage, Point(right, 0), Point(right, segmentedImage.rows), Scalar(255, 0, 0));

	imshow(wName, segmentedImage);

	while (cv::waitKey() != VK_ESCAPE) {}

	cv::destroyAllWindows();
}

int main()
{
	for (int i = 15; i < 21; i++) {
		subtractImages("background.bmp", "image" + to_string(i) + ".bmp");
	}
	return 0;
}

